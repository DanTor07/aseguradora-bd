<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Propietarios Registrados</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <style>
        /* Estilos generales */
        body {
            background: linear-gradient(45deg, #00bcd4, #009688);
            color: #000;
            font-family: 'Segoe UI', sans-serif;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 1200px;
            margin: 0 auto;
            padding: 20px;
        }

        /* Estilos para el botón de retorno */
        .return-btn {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 4px;
            padding: 10px 20px;
            cursor: pointer;
            transition: background-color 0.3s ease;
            text-decoration: none; /* Quita el subrayado del enlace */
            display: inline-block;
            position: relative; /* Cambia la posición a relativa */
            float: right; /* Alinea a la derecha */
            margin-right: 10px; /* Espacio entre el botón y el formulario */
            margin-bottom: 0px; /* Espacio debajo del botón */
            font-size: 14px; /* Tamaño de la letra del botón */
            font-weight: bold; /* Texto en negrita */
        }

        .return-btn:hover {
            background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
        }

        /* Estilos para la tabla */
        table {
            width: 100%;
            margin-top: 20px;
            border-collapse: collapse;
            border-spacing: 0;
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
        }

        th, td {
            padding: 12px 15px;
            text-align: center;
            border-bottom: 1px solid #ddd;
            color: #000;
        }

        th {
            background-color: #4CAF50;
            color: #fff;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        tr:nth-child(odd) {
            background-color: #ddd;
        }

        tr:hover {
            background: linear-gradient(45deg, #1976D2, #BBDEFB);
            color: #fff;
            transition: background-color 0.3s ease;
        }

        /* Estilos para el botón de descarga */
        .download-btn {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 4px;
            padding: 10px 20px;
            cursor: pointer;
            transition: background-color 0.3s ease;
            text-decoration: none;
            display: inline-block;
            position: relative;
            float: left;
            margin-left: 10px;
            margin-bottom: 20px;
            font-size: 14px;
            font-weight: bold;
        }

        .download-btn:hover {
            background-color: #0b7dda;
        }

        h1 {
            text-align: center;
            margin-top: 50px;
            font-size: 36px;
            color: #fff;
            margin-top: 0; /* Elimina el espacio vacío encima del encabezado */
            margin-right: 100px; /* Agrega margen derecho para mover el texto hacia la derecha */
        }

        /* Estilos para el botón de eliminar */
        .delete-btn {
            background-color: #dc3545; /* Color rojo para indicar eliminación */
            color: #ffffff;
            border: none;
            border-radius: 4px;
            padding: 6px 12px;
            cursor: pointer;
            transition: background-color 0.3s ease;
            font-size: 16px;
            font-weight: bold;
            text-transform: uppercase;
        }

        .delete-btn:hover {
            background-color: #c82333; /* Color rojo más oscuro al pasar el ratón */
        }

        /* Estilos para el botón de editar */
        .edit-btn {
            background-color: #28a745; /* Color verde para el botón de editar */
            color: #ffffff;
            border: none;
            border-radius: 4px;
            padding: 6px 12px;
            cursor: pointer;
            transition: background-color 0.3s ease;
            font-size: 16px;
            font-weight: bold;
            text-transform: uppercase;
        }

        .edit-btn:hover {
            background-color: #218838; /* Color verde más oscuro al pasar el ratón */
        }
    </style>
</head>
<body>
    <div class="container">
        <a href="registrar_propietario.php" class="return-btn">Volver a Registrar Propietario</a>
        <h1>Propietarios Registrados</h1>
        <table class="prop-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Documento Identidad</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th>Fecha de Nacimiento</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $server = 'localhost:3306'; // Tu servidor MySQL
                $username = 'root'; // Tu nombre de usuario de MySQL
                $password = ''; // Tu contraseña de MySQL
                $database ='pr_informe'; // Tu base de datos
                
                try {
                    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
                    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                    $stmt = $con->prepare("SELECT * FROM propietario");
                    $stmt->execute();
                    
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        echo "<tr>";
                        echo "<td>{$row['id_propietario']}</td>";
                        echo "<td>{$row['nombre']}</td>";
                        echo "<td>{$row['documento_identidad']}</td>";
                        echo "<td>{$row['direccion']}</td>";
                        echo "<td>{$row['telefono']}</td>";
                        echo "<td>{$row['fecha_nacimiento']}</td>";
                        echo '<td>';
                        echo '<button class="delete-btn" onclick="eliminarPropietario(' . $row['id_propietario'] . ')">Eliminar</button>';
                        echo '<button class="edit-btn" onclick="editarPropietario(' . $row['id_propietario'] . ')">Editar</button>';
                        echo '</td>';
                        echo "</tr>";
                    }
                } catch (PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }
                ?>
            </tbody>
        </table>
    </div>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <!-- Custom JavaScript -->
    <script src="custom.js"></script>
    <script>
        function eliminarPropietario(id) {
            if (confirm('¿Estás seguro de que deseas eliminar este propietario?')) {
                $.ajax({
                    type: 'POST',
                    url: 'eliminar_propietario.php',
                    data: { id_propietario: id },
                    success: function(response) {
                        alert('Propietario eliminado correctamente.');
                        location.reload(); // Recargar la página después de la eliminación
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                        alert('Error al eliminar el propietario.');
                    }
                });
            }
        }

        function editarPropietario(id) {
            // Redireccionar a la página de edición con el ID del propietario
            window.location.href = 'editar_propietario.php?id=' + id;
        }
    </script>
</body>
</html>
