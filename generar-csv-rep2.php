<?php
// Configuración de la conexión a la base de datos
$servername = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$dbname = 'pr_informe'; // Tu base de datos

try {
    // Crear conexión
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // Establecer el modo de error PDO a excepción
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Seleccionar la base de datos (no es necesario, ya que ya está seleccionada en la conexión)

    // Consulta SQL
    $sql = "SELECT p.nombre AS nombre_propietario, v.placa AS placa_vehiculo, pol.numero_poliza AS numero_poliza
            FROM propietario p
            INNER JOIN vehiculo v ON p.id_propietario = v.id_propietario
            INNER JOIN poliza pol ON pol.id_vehiculo = v.id_vehiculo";

    // Ejecutar consulta y obtener resultados
    $stmt = $conn->query($sql);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

// Cerrar conexión
$conn = null;

// Crear un archivo temporal para el CSV
$temp_file = tempnam(sys_get_temp_dir(), 'data_');

// Abre el archivo temporal en modo escritura
$csv_file = fopen($temp_file, 'w');

// Escribe los datos CSV en el archivo
if ($csv_file) {
    // Escribe el encabezado del CSV
    fputcsv($csv_file, array('Nombre', 'Placa del Vehículo', 'Número de Póliza'));

    // Escribe los datos de la tabla en el CSV
    foreach ($results as $row) {
        fputcsv($csv_file, array($row['nombre_propietario'], $row['placa_vehiculo'], $row['numero_poliza']));
    }

    // Cierra el archivo CSV
    fclose($csv_file);

    // Configura los encabezados para descargar el archivo
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="datos_tabla.csv"');

    // Lee el archivo temporal y envía su contenido al navegador para descargar
    readfile($temp_file);

    // Elimina el archivo temporal después de descargarlo
    unlink($temp_file);
    exit;
}
?>
