<?php
$server = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$database ='pr_informe'; // Tu base de datos

try {
    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if (isset($_POST['id_vehiculo'])) {
        $id_vehiculo = $_POST['id_vehiculo'];

        // Delete associated insurance policy record
        $delete_policy = $con->prepare("DELETE FROM poliza WHERE id_vehiculo = :id");
        $delete_policy->bindParam(':id', $id_vehiculo);
        $delete_policy->execute();

        // Delete vehicle record
        $delete_vehicle = $con->prepare("DELETE FROM vehiculo WHERE id_vehiculo = :id");
        $delete_vehicle->bindParam(':id', $id_vehiculo);
        $delete_vehicle->execute();

        echo "success";
        exit; // Exit after successful deletion
    }
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
