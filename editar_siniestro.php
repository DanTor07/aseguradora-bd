<?php
$server = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$database = 'pr_informe'; // Tu base de datos

try {
    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Variables para el modal
    $modal_message = '';
    $display_modal = 'none'; // Ocultar el modal inicialmente

    // Verificar si se ha enviado el formulario de edición
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $id_siniestro = $_POST["id_siniestro"];
        $zona = $_POST["zona"];
        $direccion = $_POST["direccion"];
        $victimas = $_POST["victimas"];
        $fecha = $_POST["fecha"];
        $hora = $_POST["hora"];

        $sql = "UPDATE siniestro SET zona = :zona, direccion = :direccion, victimas = :victimas, fecha = :fecha, hora = :hora WHERE id_siniestro = :id";

        $stmt = $con->prepare($sql);

        $stmt->bindParam(':zona', $zona);
        $stmt->bindParam(':direccion', $direccion);
        $stmt->bindParam(':victimas', $victimas);
        $stmt->bindParam(':fecha', $fecha);
        $stmt->bindParam(':hora', $hora);
        $stmt->bindParam(':id', $id_siniestro);

        if ($stmt->execute()) {
            $modal_message = "Siniestro actualizado correctamente.";
            $display_modal = 'block'; // Mostrar el modal
        } else {
            $modal_message = "Error al actualizar el siniestro.";
            $display_modal = 'block'; // Mostrar el modal
        }
    }

    // Obtener el ID del siniestro de la URL
    $id_siniestro = $_GET["id"];
    $stmt = $con->prepare("SELECT * FROM siniestro WHERE id_siniestro = :id");
    $stmt->bindParam(':id', $id_siniestro);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    // Mostrar el formulario de edición con los datos del siniestro
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Editar Siniestro</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <style>
            /* Estilos para el botón de retorno */
            .return-btn {
                background-color: #888; /* Color gris para el botón de retorno */
                color: #fefefe;
                border: none;
                border-radius: 4px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none; /* Quita el subrayado del enlace */
                font-size: 14px; /* Tamaño de la letra del botón */
                font-weight: bold; /* Texto en negrita */
            }

            .return-btn:hover {
                background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
            }

            /* Estilos para el contenedor de botones */
            .button-group {
                margin-top: 20px; /* Espacio superior entre el formulario y los botones */
                display: flex; /* Utiliza flexbox para alinear horizontalmente los elementos */
                justify-content: space-between; /* Espacio uniforme entre los elementos */
            }

            /* Estilos para el modal */
            .modal {
                display: <?php echo $display_modal; ?>; /* Controla la visibilidad del modal */
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.5);
            }

            .modal-content {
                background-color: #fefefe;
                margin: 15% auto;
                padding: 20px;
                border: 1px solid #888;
                border-radius: 10px;
                width: 80%;
                max-width: 400px;
            }

            .close-btn {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close-btn:hover,
            .close-btn:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

            /* Estilos para el formulario y el modal */
            body {
                font-family: Arial, sans-serif;
                background-color: #f1f1f1; /* Color de fondo general */
            }

            .form-container {
                background-color: #ddd; /* Fondo blanco del formulario */
                max-width: 500px;
                margin: 80px auto;
                padding: 30px;
                border-radius: 10px;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); /* Sombra suave alrededor del formulario */
            }

            h2 {
                color: #4caf50; /* Color azul del encabezado */
            }

            .form-group {
                margin-bottom: 20px;
            }

            .form-label {
                display: block;
                margin-bottom: 5px;
                color: #555555; /* Color de texto gris para las etiquetas */
            }

            .form-input {
                width: 100%;
                padding: 10px;
                border: 1px solid #797979; /* Borde gris claro para los campos de entrada */
                border-radius: 5px;
                box-sizing: border-box;
            }

            .form-button {
                background-color: #4CAF50;
                color: #fff;
                border: none;
                border-radius: 4px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none;
                display: inline-block;
                position: relative;
                float: right;
                margin-right: 10px;
                margin-bottom: 0px;
                font-size: 14px;
                font-weight: bold;
            }

            .form-button:hover {
                background-color: #0056b3;
            }

            body {
            background-image: url("img/editSiniestro.jpg"); /* Ruta de tu imagen de fondo */
            background-size: cover; /* Ajusta la imagen para cubrir todo el fondo */
            background-position: center; /* Centra la imagen en el fondo */
            background-repeat: no-repeat; /* Evita que la imagen se repita */
            }

        </style>
    </head>
    <body>
        <div class="form-container">
            <h2 style="text-align: center;">Editar Siniestro</h2>
            <form method="POST" action="">
                <input type="hidden" name="id_siniestro" value="<?php echo $row['id_siniestro']; ?>">
                <div class="form-group">
                    <label for="zona" class="form-label">Zona:</label>
                    <input type="text" id="zona" name="zona" class="form-input" value="<?php echo $row['zona']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="direccion" class="form-label">Dirección:</label>
                    <input type="text" id="direccion" name="direccion" class="form-input" value="<?php echo $row['direccion']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="victimas" class="form-label">Víctimas:</label>
                    <input type="number" id="victimas" name="victimas" class="form-input" value="<?php echo $row['victimas']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="fecha" class="form-label">Fecha:</label>
                    <input type="date" id="fecha" name="fecha" class="form-input" value="<?php echo $row['fecha']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="hora" class="form-label">Hora:</label>
                    <input type="time" id="hora" name="hora" class="form-input" value="<?php echo $row['hora']; ?>" required>
                </div>
                <div class="form-group button-group">
                    <button type="submit" class="form-button">Actualizar Siniestro</button>
                    <a href="visualizar_siniestro.php" class="return-btn">Volver a Siniestros</a>
                </div>
            </form>
            <!-- Modal -->
            <div id="myModal" class="modal">
                <div class="modal-content">
                    <span class="close-btn">&times;</span>
                    <p><?php echo $modal_message; ?></p>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
            // Cerrar el modal al hacer clic en la 'X'
            $(".close-btn").click(function() {
                $(".modal").hide();
            });

            // Cerrar el modal al hacer clic fuera del contenido del modal
            window.onclick = function(event) {
                var modal = document.getElementById('myModal');
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            };
        </script>
    </body>
    </html>
    <?php
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
}
?>
