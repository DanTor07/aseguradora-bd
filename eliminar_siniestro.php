<?php
$server = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$database = 'pr_informe'; // Tu base de datos

try {
    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id_siniestro'])) {
        $id_siniestro = $_POST['id_siniestro'];

        $delete_siniestro = $con->prepare("DELETE FROM siniestro WHERE id_siniestro = :id");
        $delete_siniestro->bindParam(':id', $id_siniestro);

        if ($delete_siniestro->execute()) {
            echo "Siniestro eliminado correctamente.";
        } else {
            echo "Error al eliminar el siniestro.";
        }
    }
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
}
?>
