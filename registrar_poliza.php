<?php
$server = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$database = 'pr_informe'; // Tu base de datos

try {
    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Variables para el modal
    $modal_message = '';
    $display_modal = 'none'; // Ocultar el modal inicialmente

    // Verificar si se ha enviado el formulario de registro
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero_poliza = $_POST["numero_poliza"];
        $fecha_inicio = $_POST["fecha_inicio"];
        $fecha_fin = $_POST["fecha_fin"];
        $codigo_aseguradora = $_POST["codigo_aseguradora"];
        $precio = $_POST["precio"];
        $placa = $_POST["placa"];

        // Verificar si la placa del vehículo existe en la base de datos
        $stmt_vehiculo = $con->prepare("SELECT id_vehiculo FROM vehiculo WHERE placa = ?");
        $stmt_vehiculo->execute([$placa]);
        $row_vehiculo = $stmt_vehiculo->fetch(PDO::FETCH_ASSOC);

        if ($row_vehiculo && isset($row_vehiculo['id_vehiculo'])) {
            // Obtener el id_vehiculo asociado a la placa ingresada
            $id_vehiculo = $row_vehiculo['id_vehiculo'];

            // Preparar la consulta SQL para insertar la póliza
            $sql = "INSERT INTO poliza (numero_poliza, fecha_inicio, fecha_fin, codigo_aseguradora, precio, id_vehiculo) 
                    VALUES (:numero_poliza, :fecha_inicio, :fecha_fin, :codigo_aseguradora, :precio, :id_vehiculo)";
            
            $stmt = $con->prepare($sql);
            
            $stmt->bindParam(':numero_poliza', $numero_poliza);
            $stmt->bindParam(':fecha_inicio', $fecha_inicio);
            $stmt->bindParam(':fecha_fin', $fecha_fin);
            $stmt->bindParam(':codigo_aseguradora', $codigo_aseguradora);
            $stmt->bindParam(':precio', $precio);
            $stmt->bindParam(':id_vehiculo', $id_vehiculo);
            
            if ($stmt->execute()) {
                $modal_message = "Póliza registrada correctamente.";
            } else {
                $modal_message = "Error al registrar la póliza.";
            }
        } else {
            $modal_message = "Error: La placa del vehículo no existe.";
        }

        $display_modal = 'block'; // Mostrar el modal
    }

    // Mostrar el formulario de registro de la póliza
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Registrar Póliza</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <style>
            /* Estilos para el formulario y el modal */
            body {
                font-family: Arial, sans-serif;
                background-color: #f1f1f1; /* Color de fondo general */
                background-image: url("img/poliza.jpg"); /* Ruta de tu imagen de fondo */
                background-size: cover; /* Ajusta la imagen para cubrir todo el fondo */
                background-position: center; /* Centra la imagen en el fondo */
                background-repeat: no-repeat; /* Evita que la imagen se repita */
            }

            .form-container {
                background-color: #ddd; /* Fondo blanco del formulario */
                max-width: 500px;
                margin: 80px auto;
                padding: 30px;
                border-radius: 10px;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); /* Sombra suave alrededor del formulario */
            }
            h2 {
                color: #4caf50; /* Color azul del encabezado */
            }

            .form-group {
                margin-bottom: 20px;
            }

            .form-label {
                display: block;
                margin-bottom: 5px;
                color: #555555; /* Color de texto gris para las etiquetas */
            }

            .form-input {
                width: 100%;
                padding: 10px;
                border: 1px solid #797979; /* Borde gris claro para los campos de entrada */
                border-radius: 5px;
                box-sizing: border-box;
            }


            .form-button {
                background-color: #4CAF50;
                color: #fff;
                border: none;
                border-radius: 4px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none;
                display: inline-block;
                position: relative;
                font-weight: bold;
                font-size: 14px;

            }

            .form-button:hover {
                background-color: #0056b3;
            }

            /* Estilos para el modal */
            .modal {
                display: <?php echo $display_modal; ?>; /* Controla la visibilidad del modal */
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.5);
                overflow: auto;
            }

            .modal-content {
                background-color: #ffffff;
                margin: 15% auto;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); /* Sombra suave alrededor del modal */
                max-width: 400px;
            }

            .close-btn {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close-btn:hover,
            .close-btn:focus {
                color: #000000;
                text-decoration: none;
                cursor: pointer;
            }

            /* Estilos para el botón de retorno */
            .return-btn {
                background-color: #888; /* Color gris para el botón de retorno */
                color: #fefefe;
                border: none;
                border-radius: 4px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none; /* Quita el subrayado del enlace */
                display: inline-block;
                position: relative; /* Cambia la posición a relativa */
                float: right; /* Alinea a la derecha */
                margin-right: 10px; /* Espacio entre el botón y el formulario */
                margin-bottom: 0px; /* Espacio debajo del botón */
                font-size: 14px; /* Tamaño de la letra del botón */
                font-weight: bold; /* Texto en negrita */
            }

            .return-btn:hover {
                background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
            }

            /* Estilos para los nuevos botones */
            /* Estilos para los nuevos botones */
            .nav-btn {
                background-color: #4CAF50;
                color: #fff;
                border: none;
                border-radius: 5px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none;
                display: inline-block;
                position: absolute;
                top: 50%; /* Ajusta la posición vertical */
                transform: translateY(-50%); /* Ajuste para el centrado vertical */
                font-weight: bold;
                font-size: 14px;    
            }

            .form-button:hover {
                background-color: #0056b3;
            }

            .nav-btn.left {
                left: 130px; /* Ajusta la posición a la izquierda */
            }

            .nav-btn.right {
                right: 130px; /* Ajusta la posición a la derecha */
            }

            .nav-btn:hover {
                background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
            }
        </style>
    </head>
    <body>
        <a href="http://localhost/aseguradora-bd/iniciar-sesion.html" class="return-btn">Página Principal</a>
        
        <div class="form-container">
            <h2 style="text-align: center;">Registrar Póliza</h2>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="numero_poliza" class="form-label">Número de Póliza:</label>
                    <input type="text" id="numero_poliza" name="numero_poliza" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="fecha_inicio" class="form-label">Fecha de Inicio:</label>
                    <input type="date" id="fecha_inicio" name="fecha_inicio" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="fecha_fin" class="form-label">Fecha de Fin:</label>
                    <input type="date" id="fecha_fin" name="fecha_fin" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="codigo_aseguradora" class="form-label">Código de la Aseguradora:</label>
                    <input type="text" id="codigo_aseguradora" name="codigo_aseguradora" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="precio" class="form-label">Precio:</label>
                    <input type="text" id="precio" name="precio" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="placa" class="form-label">Placa del Vehículo:</label>
                    <input type="text" id="placa" name="placa" class="form-input" required>
                </div>
                <div class="form-group button-group">
                    <button type="submit" class="form-button">Registrar Póliza</button>
                    <a href="visualizar_polizas.php" class="return-btn">Ver Pólizas</a>
                </div>
            </form>
            <!-- Modal -->
            <div id="myModal" class="modal" style="display: <?php echo $display_modal; ?>;">
                <div class="modal-content">
                    <span class="close-btn">&times;</span>
                    <p><?php echo $modal_message; ?></p>
                </div>
            <div>

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
            $(document).ready(function(){
                // Cerrar el modal al hacer clic en la X
                $(".close-btn").click(function(){
                    $(".modal").hide();
                });

                // Cerrar el modal al hacer clic fuera del contenido
                window.onclick = function(event) {
                    if (event.target == document.getElementById('myModal')) {
                        $(".modal").hide();
                    }
                };
            });
        </script>
    </body>
    </html>
    <?php
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
