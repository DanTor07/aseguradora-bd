<?php
$server = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$database = 'pr_informe'; // Tu base de datos

try {
    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id_propietario'])) {
        $id_propietario = $_POST['id_propietario'];

        $delete_propietario = $con->prepare("DELETE FROM propietario WHERE id_propietario = :id");
        $delete_propietario->bindParam(':id', $id_propietario);
        
        if ($delete_propietario->execute()) {
            echo "Propietario eliminado correctamente.";
        } else {
            echo "Error al eliminar el propietario.";
        }
    }
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
}
?>
