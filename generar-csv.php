<?php
// Database connection parameters
$servername = 'localhost:3306';
$username = 'root';
$password = '';
$dbname = 'pr_informe';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Error de conexión: " . $conn->connect_error);
}

// SQL query to fetch the required data
$sql = "SELECT 
            prop.nombre AS Nombre_Propietario, 
            prop.documento_identidad AS Documento_Propietario,
            veh.placa AS Placa_Vehiculo, 
            CONCAT(DATE_FORMAT(pol.fecha_inicio, '%d-%b-%Y'), ' / ', DATE_FORMAT(pol.fecha_fin, '%d-%b-%Y')) AS Fecha_Poliza,
            pol.numero_poliza AS Numero_Poliza
        FROM 
            propietario prop
        JOIN 
            propietario_vehiculo pv ON prop.id_propietario = pv.id_propietario
        JOIN 
            vehiculo veh ON pv.id_vehiculo = veh.id_vehiculo
        JOIN 
            poliza pol ON veh.id_vehiculo = pol.id_vehiculo
        WHERE 
            pol.numero_poliza != 'PZ009'";      

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Set headers for CSV download
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="historico_propietarios_vehiculos.csv"');

    // Open file handle to write CSV data
    $output = fopen('php://output', 'w');

    // Write CSV headers
    fputcsv($output, array('Nombre_Propietario', 'Documento_Propietario', 'Placa_Vehiculo', 'Fecha_Poliza', 'Numero_Poliza'), "\t");

    // Output data of each row
    while($row = $result->fetch_assoc()) {
        // Write each row as CSV data with tab delimiter
        fputcsv($output, $row, "\t");
    }

    // Close file handle
    fclose($output);
} else {
    echo "0 resultados";
}

// Close the connection
$conn->close();
?>
