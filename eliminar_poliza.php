<?php
$server = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$database = 'pr_informe'; // Tu base de datos

try {
    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['id_poliza'])) {
        $id_poliza = $_POST['id_poliza'];

        $delete_poliza = $con->prepare("DELETE FROM poliza WHERE id_poliza = :id");
        $delete_poliza->bindParam(':id', $id_poliza);

        if ($delete_poliza->execute()) {
            echo "Póliza eliminada correctamente.";
        } else {
            echo "Error al eliminar la póliza.";
        }
    }
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
}
?>
