<?php
$server = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$database ='pr_informe'; // Tu base de datos

try {
    // Conexión a la base de datos
    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Variables para el modal
    $modal_message = '';
    $display_modal = 'none'; // Ocultar el modal inicialmente

    // Verificar si se ha enviado el formulario de registro de siniestro
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["submit"])) {
        $zona = $_POST['zona'];
        $direccion = $_POST['direccion'];
        $victimas = $_POST['victimas'];
        $fecha = $_POST['fecha'];
        $hora = $_POST['hora'];
        $placa = $_POST['placa'];

        // Consulta SQL para obtener el id_vehiculo asociado a la placa ingresada
        $stmt_vehiculo = $con->prepare("SELECT id_vehiculo FROM vehiculo WHERE placa = ?");
        $stmt_vehiculo->execute([$placa]);
        $row_vehiculo = $stmt_vehiculo->fetch(PDO::FETCH_ASSOC);

        // Mostrar los resultados de la consulta para depuración
        var_dump($row_vehiculo);

        // Verificar si se encontró el id_vehiculo
        if ($row_vehiculo && isset($row_vehiculo['id_vehiculo'])) {
            $id_vehiculo = $row_vehiculo['id_vehiculo'];
            // Preparar la consulta SQL para insertar el siniestro
            $stmt = $con->prepare("INSERT INTO siniestro (zona, direccion, victimas, fecha, hora, id_vehiculo) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->execute([$zona, $direccion, $victimas, $fecha, $hora, $id_vehiculo]);

            // Mostrar el modal con el mensaje de éxito
            $modal_message = "Siniestro registrado correctamente.";
        } else {
            // Mostrar el modal con el mensaje de error
            $modal_message = "Error: La placa ingresada no se encuentra registrada.";
        }

        // Mostrar el modal
        $display_modal = 'block';
    }

    // Mostrar el formulario de registro de siniestro
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Registrar Siniestro</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <style>
            /* Estilos para el formulario y el modal */
            body {
                font-family: Arial, sans-serif;
                background-color: #f1f1f1; /* Color de fondo general */
            }

            .form-container {
                background-color: #ddd; /* Fondo blanco del formulario */
                max-width: 500px;
                margin: 80px auto;
                padding: 30px;
                border-radius: 10px;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); /* Sombra suave alrededor del formulario */
            }
            h2 {
                color: #4caf50; /* Color azul del encabezado */
            }
            .form-group {
                margin-bottom: 20px;
            }

            .form-label {
                display: block;
                margin-bottom: 5px;
                color: #555555; /* Color de texto gris para las etiquetas */
            }
            .form-input {
                width: 100%;
                padding: 10px;
                border: 1px solid #797979; /* Borde gris claro para los campos de entrada */
                border-radius: 5px;
                box-sizing: border-box;
            }


            .form-button {
                background-color: #4CAF50;
                color: #fff;
                border: none;
                border-radius: 4px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none;
                display: inline-block;
                position: relative;
                font-weight: bold;
                font-size: 14px;

            }

            .form-button:hover {
                background-color: #0056b3;
            }

            .return-btn {
                background-color: #888; /* Color gris para el botón de retorno */
                color: #fefefe;
                border: none;
                border-radius: 4px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none; /* Quita el subrayado del enlace */
                display: inline-block;
                position: relative; /* Cambia la posición a relativa */
                float: right; /* Alinea a la derecha */
                margin-right: 10px; /* Espacio entre el botón y el formulario */
                margin-bottom: 0px; /* Espacio debajo del botón */
                font-size: 14px; /* Tamaño de la letra del botón */
                font-weight: bold; /* Texto en negrita */
            }

            .return-btn:hover {
                background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
            }
            .form-button:hover {
                background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
            }



            /* Estilos para el modal */
            .modal {
                display: <?php echo $display_modal; ?>; /* Controla la visibilidad del modal */
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.5);
                overflow: auto;
            }

            .modal-content {
                background-color: #ffffff;
                margin: 15% auto;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); /* Sombra suave alrededor del modal */
                max-width: 400px;
            }

            .close-btn {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close-btn:hover,
            .close-btn:focus {
                color: #000000;
                text-decoration: none;
                cursor: pointer;
            }

            body {
            background-image: url("img/accidente.jpg"); /* Ruta de tu imagen de fondo */
            background-size: cover; /* Ajusta la imagen para cubrir todo el fondo */
            background-position: center; /* Centra la imagen en el fondo */
            background-repeat: no-repeat; /* Evita que la imagen se repita */
            }

        </style>
    </head>
    <body>
        <a href="http://localhost/aseguradora-bd/iniciar-sesion.html" class="return-btn">Página Principal</a>
        <div class="form-container">
            <h2 style="text-align: center;">Registrar Siniestro</h2>
            <form method="POST" action="">
                <div class="form-group">
                    <label for="placa" class="form-label">Placa:</label>
                    <input type="text" id="placa" name="placa" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="zona" class="form-label">Zona:</label>
                    <input type="text" id="zona" name="zona" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="direccion" class="form-label">Dirección:</label>
                    <input type="text" id="direccion" name="direccion" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="victimas" class="form-label">Víctimas:</label>
                    <input type="number" id="victimas" name="victimas" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="fecha" class="form-label">Fecha:</label>
                    <input type="date" id="fecha" name="fecha" class="form-input" required>
                </div>
                <div class="form-group">
                    <label for="hora" class="form-label">Hora:</label>
                    <input type="time" id="hora" name="hora" class="form-input" required>
                </div>
                <button type="submit" name="submit" class="form-button">Registrar Siniestro</button>
                <a href="visualizar_siniestro.php" class="return-btn">Ver Siniestro</a>
            </form>
            <!-- Modal -->
            <div id="myModal" class="modal" style="display: <?php echo $display_modal; ?>;">
                <div class="modal-content">
                    <span class="close-btn">&times;</span>
                    <p><?php echo $modal_message; ?></p>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
            // Cerrar el modal al hacer clic en la 'X'
            $(".close-btn").click(function() {
                $(".modal").hide();
            });

            // Cerrar el modal al hacer clic fuera del contenido del modal
            window.onclick = function(event) {
                var modal = document.getElementById('myModal');
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            };
        </script>
    </body>
    </html>
    <?php
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
}
?>
