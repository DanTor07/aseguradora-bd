<?php
$server = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$database = 'pr_informe'; // Tu base de datos

try {
    $con = new PDO("mysql:host=$server;dbname=$database;", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Variables para el modal
    $modal_message = '';
    $display_modal = 'none'; // Ocultar el modal inicialmente

    // Verificar si se ha enviado el formulario de edición
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $id = $_POST["id_propietario"];
        $nombre = $_POST["nombre"];
        $documento = $_POST["documento"];
        $direccion = $_POST["direccion"] ?? null;
        $telefono = $_POST["telefono"] ?? null;
        $fecha_nacimiento = $_POST["fecha_nacimiento"];

        $sql = "UPDATE propietario SET nombre = :nombre, documento_identidad = :documento, direccion = :direccion, telefono = :telefono, fecha_nacimiento = :fecha_nacimiento WHERE id_propietario = :id";
        
        $stmt = $con->prepare($sql);
        
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':documento', $documento);
        $stmt->bindParam(':direccion', $direccion);
        $stmt->bindParam(':telefono', $telefono);
        $stmt->bindParam(':fecha_nacimiento', $fecha_nacimiento);
        $stmt->bindParam(':id', $id);
        
        if ($stmt->execute()) {
            $modal_message = "Propietario actualizado correctamente.";
            $display_modal = 'block'; // Mostrar el modal
        } else {
            $modal_message = "Error al actualizar el propietario.";
            $display_modal = 'block'; // Mostrar el modal
        }
    }

    // Obtener el ID del propietario de la URL
    $id_propietario = $_GET["id"];
    $stmt = $con->prepare("SELECT * FROM propietario WHERE id_propietario = :id");
    $stmt->bindParam(':id', $id_propietario);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    // Mostrar el formulario de edición con los datos del propietario
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Editar Propietario</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <style>
            /* Estilos para el botón de retorno */
            .return-btn {
                background-color: #888; /* Color gris para el botón de retorno */
                color: #fefefe;
                border: none;
                border-radius: 4px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none; /* Quita el subrayado del enlace */
                font-size: 14px; /* Tamaño de la letra del botón */
                font-weight: bold; /* Texto en negrita */
            }

            .return-btn:hover {
                background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
            }

            /* Estilos para el contenedor de botones */
            .button-group {
                margin-top: 20px; /* Espacio superior entre el formulario y los botones */
                display: flex; /* Utiliza flexbox para alinear horizontalmente los elementos */
                justify-content: space-between; /* Espacio uniforme entre los elementos */
            }

            /* Estilos para el modal */
            .modal {
                display: <?php echo $display_modal; ?>; /* Controla la visibilidad del modal */
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.5);
            }

            .modal-content {
                background-color: #fefefe;
                margin: 15% auto;
                padding: 20px;
                border: 1px solid #888;
                border-radius: 10px;
                width: 80%;
                max-width: 400px;
            }

            .close-btn {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close-btn:hover,
            .close-btn:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

            /* Estilos para el formulario y el modal */
            body {
                font-family: Arial, sans-serif;
                background-color: #f1f1f1; /* Color de fondo general */
            }

            .form-container {
                background-color: #ddd; /* Fondo blanco del formulario */
                max-width: 500px;
                margin: 80px auto;
                padding: 30px;
                border-radius: 10px;
                box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1); /* Sombra suave alrededor del formulario */
            }

            h2 {
                color: #4caf50; /* Color azul del encabezado */
            }


            .form-group {
                margin-bottom: 20px;
            }

            .form-label {
                display: block;
                margin-bottom: 5px;
                color: #555555; /* Color de texto gris para las etiquetas */
            }

            .form-input {
                width: 100%;
                padding: 10px;
                border: 1px solid #797979; /* Borde gris claro para los campos de entrada */
                border-radius: 5px;
                box-sizing: border-box;
            }

            .form-button {
                background-color: #4CAF50;
                color: #fff;
                border: none;
                border-radius: 4px;
                padding: 10px 20px;
                cursor: pointer;
                transition: background-color 0.3s ease;
                text-decoration: none;
                display: inline-block;
                position: relative;
                float: right;
                margin-right: 10px;
                margin-bottom: 0px;
                font-size: 14px;
                font-weight: bold;
            }

            .form-button:hover {
                background-color: #0056b3;
            }

            /* Estilos para el formulario y el modal */
            body {
                font-family: Arial, sans-serif;
                background-color: #f1f1f1; /* Color de fondo general */
                background-image: url("img/editPropietario.jpg"); /* Ruta de tu imagen de fondo */
                background-size: cover; /* Ajusta la imagen para cubrir todo el fondo */
                background-position: center; /* Centra la imagen en el fondo */
                background-repeat: no-repeat; /* Evita que la imagen se repita */
            }
        </style>
    </head>
    <body>
        <div class="form-container">
            <h2 style="text-align: center;">Editar Propietario</h2>
            <form method="POST" action="">
                <input type="hidden" name="id_propietario" value="<?php echo $row['id_propietario']; ?>">
                <div class="form-group">
                    <label for="nombre" class="form-label">Nombre:</label>
                    <input type="text" id="nombre" name="nombre" class="form-input" value="<?php echo $row['nombre']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="documento" class="form-label">Documento Identidad:</label>
                    <input type="text" id="documento" name="documento" class="form-input" value="<?php echo $row['documento_identidad']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="direccion" class="form-label">Dirección:</label>
                    <input type="text" id="direccion" name="direccion" class="form-input" value="<?php echo $row['direccion']; ?>">
                </div>
                <div class="form-group">
                    <label for="telefono" class="form-label">Teléfono:</label>
                    <input type="text" id="telefono" name="telefono" class="form-input" value="<?php echo $row['telefono']; ?>">
                </div>
                <div class="form-group">
                    <label for="fecha_nacimiento" class="form-label">Fecha de Nacimiento:</label>
                    <input type="date" id="fecha_nacimiento" name="fecha_nacimiento" class="form-input" value="<?php echo $row['fecha_nacimiento']; ?>" required>
                </div>
                <div class="form-group button-group">
                <button type="submit" class="form-button">Actualizar Propietario</button>
                <a href="visualizar_propietarios.php" class="return-btn">Volver a Propietarios</a>
                </div>
            </form>
            <!-- Modal -->
            <div id="myModal" class="modal">
                <div class="modal-content">
                    <span class="close-btn">&times;</span>
                    <p><?php echo $modal_message; ?></p>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <!-- Custom JavaScript -->
        <script src="custom.js"></script>
        <script>
            // Cerrar el modal al hacer clic en la 'X'
            $(".close-btn").click(function() {
                $(".modal").hide();
            });

            // Cerrar el modal al hacer clic fuera del contenido del modal
            window.onclick = function(event) {
                var modal = document.getElementById('myModal');
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            };
        </script>
        <script>
            function vaciarCampos() {
                document.getElementById("nombre").value = "";
                document.getElementById("documento").value = "";
                document.getElementById("direccion").value = "";
                document.getElementById("telefono").value = "";
                document.getElementById("fecha_nacimiento").value = "";
            }
        </script>
    </body>
    </html>
    <?php
} catch (PDOException $e) {
    echo "Error de conexión: " . $e->getMessage();
}
?>
