<?php
// Database connection parameters
$servername = 'localhost:3306';
$username = 'root';
$password = '';
$dbname = 'pr_informe';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Error de conexión: " . $conn->connect_error);
}

// SQL query to fetch clients without incidents
$sql = "SELECT p.nombre, p.documento_identidad 
        FROM propietario p 
        LEFT JOIN vehiculo v ON p.id_propietario = v.id_propietario 
        LEFT JOIN siniestro s ON s.id_vehiculo = v.id_vehiculo 
        WHERE s.id_siniestro IS NULL";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Set headers for CSV download
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="clientes_sin_siniestro.csv"');

    // Open file handle to write CSV data
    $output = fopen('php://output', 'w');

    // Write CSV headers
    fputcsv($output, array('Nombre', 'Documento Identidad'));

    // Output data of each row
    while($row = $result->fetch_assoc()) {
        // Write each row as CSV data
        fputcsv($output, $row);
    }

    // Close file handle
    fclose($output);
} else {
    echo "0 resultados";
}

// Close the connection
$conn->close();
?>
