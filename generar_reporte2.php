<?php
// Configuración de la conexión a la base de datos
$servername = 'localhost:3306'; // Tu servidor MySQL
$username = 'root'; // Tu nombre de usuario de MySQL
$password = ''; // Tu contraseña de MySQL
$dbname = 'pr_informe'; // Tu base de datos

try {
    // Crear conexión
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // Establecer el modo de error PDO a excepción
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Seleccionar la base de datos (no es necesario, ya que ya está seleccionada en la conexión)

    // Consulta SQL
    $sql = "SELECT p.nombre AS nombre_propietario, v.placa AS placa_vehiculo, pol.numero_poliza AS numero_poliza
            FROM propietario p
            INNER JOIN vehiculo v ON p.id_propietario = v.id_propietario
            INNER JOIN poliza pol ON pol.id_vehiculo = v.id_vehiculo";

    // Ejecutar consulta y obtener resultados
    $stmt = $conn->query($sql);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

// Cerrar conexión
$conn = null;
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de Clientes con Póliza y Placa</title>
    <style>
        /* Estilos generales */
        body {
            background: linear-gradient(45deg, #00bcd4, #009688);
            color: #000;
            font-family: 'Segoe UI', sans-serif;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 1200px;
            margin: 0 auto;
            padding: 20px;
        }

        /* Estilos para el botón de retorno */
        .return-btn {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 4px;
            padding: 10px 20px;
            cursor: pointer;
            transition: background-color 0.3s ease;
            text-decoration: none; /* Quita el subrayado del enlace */
            display: inline-block;
            position: relative; /* Cambia la posición a relativa */
            float: right; /* Alinea a la derecha */
            margin-right: 10px; /* Espacio entre el botón y el formulario */
            margin-bottom: 0px; /* Espacio debajo del botón */
            font-size: 14px; /* Tamaño de la letra del botón */
            font-weight: bold; /* Texto en negrita */
        }


        .return-btn:hover {
            background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
        }


        /* Estilos para la tabla */
        table {
            width: 100%;
            margin-top: 20px;
            border-collapse: collapse;
            border-spacing: 0;
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
        }
        th, td {
            padding: 12px 15px;
            text-align: center;
            border-bottom: 1px solid #ddd;
            color: #000;
        }
        th {
            background-color: #4CAF50;
            color: #fff;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        tr:nth-child(odd) {
            background-color: #ddd;
        }
        tr:hover {
            background: linear-gradient(45deg, #1976D2, #BBDEFB);
            color: #fff;
            transition: background-color 0.3s ease;
        }

        /* Estilos para el botón de descarga */
        .download-btn {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 4px;
            padding: 10px 20px;
            cursor: pointer;
            transition: background-color 0.3s ease;
            text-decoration: none;
            display: inline-block;
            position: relative;
            float: left;
            margin-left: 10px;
            margin-bottom: 20px;
            font-size: 14px;
            font-weight: bold;
        }

        .download-btn:hover {
            background-color: #0b7dda;
        }


        h1 {
            text-align: center;
            margin-top: 50px;
            font-size: 36px;
            color: #fff;
            margin-top: 0; /* Elimina el espacio vacío encima del encabezado */
        }
    </style>
</head>
<body>
    <div class="container">
        <a href="http://localhost/aseguradora-bd/iniciar-sesion.html" class="return-btn">Página Principal</a>
        <a href="generar-csv-rep2.php" class="download-btn">Descargar</a>
        <h1>Listado de Clientes con Póliza y Placa</h1>
        <table>
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Placa del Vehículo</th>
                    <th>Número de Póliza</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $row): ?>
                    <tr>
                        <td><?php echo $row['nombre_propietario']; ?></td>
                        <td><?php echo $row['placa_vehiculo']; ?></td>
                        <td><?php echo $row['numero_poliza']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</body>
</html>
