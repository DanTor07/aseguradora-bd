<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            background: linear-gradient(45deg, #00bcd4, #009688);
            color: #000;
            font-family: 'Segoe UI', sans-serif;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 1200px;
            margin: 0 auto;
            padding: 20px;
        }
        /* Estilos para el botón de retorno */
        .return-btn {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 4px;
            padding: 10px 20px;
            cursor: pointer;
            transition: background-color 0.3s ease;
            text-decoration: none; /* Quita el subrayado del enlace */
            display: inline-block;
            position: relative; /* Cambia la posición a relativa */
            float: right; /* Alinea a la derecha */
            margin-right: 10px; /* Espacio entre el botón y el formulario */
            margin-bottom: 0px; /* Espacio debajo del botón */
            font-size: 14px; /* Tamaño de la letra del botón */
            font-weight: bold; /* Texto en negrita */
        }


        .return-btn:hover {
            background-color: #0056b3; /* Cambio de color al pasar el mouse sobre el botón */
        }

        h1 {
            text-align: center;
            margin-top: 50px;
            font-size: 36px;
            color: #fff;
            margin-top: 0; /* Elimina el espacio vacío encima del encabezado */
        }
        table {
            width: 100%;
            margin-top: 20px;
            border-collapse: collapse;
            border-spacing: 0;
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
        }
        th, td {
            padding: 12px 15px;
            text-align: center; /* Cambio a centrado */
            border-bottom: 1px solid #ddd;
            color: #000;
        }
        th {
            background-color: #4CAF50;
            color: #fff;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        tr:nth-child(odd) {
            background-color: #ddd;
        }
        tr:hover {
            background: linear-gradient(45deg, #1976D2, #BBDEFB);
            color: #fff; /* Cambio de color del texto al pasar el mouse */
            transition: background-color 0.3s ease;
        }

        .download-btn {
        background-color: #4CAF50;
        color: #fff;
        border: none;
        border-radius: 4px;
        padding: 10px 20px;
        cursor: pointer;
        transition: background-color 0.3s ease;
        text-decoration: none;
        display: inline-block;
        position: relative;
        float: left; /* Align to the left */
        margin-left: 10px; /* Space between the button and other elements */
        margin-bottom: 20px; /* Space below the button */
        font-size: 14px;
        font-weight: bold;
        }

        .download-btn:hover {
            background-color: #0b7dda;
        }


    </style>
</head>
<body>
    <div class="container">
        <a href="http://localhost/aseguradora-bd/iniciar-sesion.html" class="return-btn">Volver</a>
        <a href="generar-csv.php" class="download-btn">Descargar</a>
        <h1>Histórico Propietario Vehiculo</h1>
        <?php
        // Database connection parameters
        $servername = 'localhost:3306';
        $username = 'root';
        $password = '';
        $dbname = 'pr_informe';

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Error de conexión: " . $conn->connect_error);
        }

        // SQL query to fetch the required data
        $sql = "SELECT 
                    prop.nombre AS Nombre_Propietario, 
                    prop.documento_identidad AS Documento_Propietario,
                    veh.placa AS Placa_Vehiculo, 
                    CONCAT(DATE_FORMAT(pol.fecha_inicio, '%d-%b-%Y'), ' / ', DATE_FORMAT(pol.fecha_fin, '%d-%b-%Y')) AS Fecha_Poliza,
                    pol.numero_poliza AS Numero_Poliza
                FROM 
                    propietario prop
                JOIN 
                    propietario_vehiculo pv ON prop.id_propietario = pv.id_propietario
                JOIN 
                    vehiculo veh ON pv.id_vehiculo = veh.id_vehiculo
                JOIN 
                    poliza pol ON veh.id_vehiculo = pol.id_vehiculo
                WHERE 
                    pol.numero_poliza != 'PZ009'";      

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            echo "<table><tr><th>Nombre Propietario</th><th>Documento Propietario</th><th>Placa Vehículo</th><th>Fecha Poliza</th><th>Numero Poliza</th></tr>";
            // Output data of each row
            while($row = $result->fetch_assoc()) {
                echo "<tr><td>".$row["Nombre_Propietario"]."</td><td>".$row["Documento_Propietario"]."</td><td>".$row["Placa_Vehiculo"]."</td><td>".$row["Fecha_Poliza"]."</td><td>".$row["Numero_Poliza"]."</td></tr>";
            }
            echo "</table>";
        } else {
            echo "0 resultados";
        }

        // Close the connection
        $conn->close();
        ?>
    </div>
</body>
</html>
